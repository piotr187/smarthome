package smarthome;

import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class smartHome {
	
	
	public void menuSwitch(Data data){
		int keyboard;
		Scanner read = new Scanner(System.in);
		boolean flag = true;
		Executor exec = Executors.newCachedThreadPool();
		int vCase = 0;
		
		while(flag){
			System.out.println("1. Ustaw temperatur�");
			System.out.println("2. Ustaw wilgotno��");
			System.out.println("3. Wyj�cie z programu");
			
			vCase = read.nextInt();
			
			switch (vCase){
			case 1: {
				System.out.println("Jak� temperatur� chcesz ustawi�? Obecnie jest: "+data.getCurrentTemperature()+"\"C");
				keyboard = read.nextInt(); 
				exec.execute(new TemperatureThread(data, keyboard));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			}
			
			case 2: {
				System.out.println("Jak� wilgotno�� chcesz ustawi�? Obecnie jest "+ data.getCurrentHumidity()+"%");
				keyboard = read.nextInt();
				exec.execute(new HumidityThread(data, keyboard));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			}
			
			case 3: {
				System.out.println("Exit");
				flag = false;
				break;
			}
			
			default: {
				System.out.println("Nieprawid�owy wyb�r. Spr�buj jeszcze raz");
				break;
			}	
		}
	}
	}

	public static void main(String[] args) {
		
		smartHome sm = new smartHome();
		Data data = new Data(24, 55);
		sm.menuSwitch(data);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
//Kopia zapasowa powy�szego kodu		
		
//		int keyboard;
//		int vCase;
//		boolean flag = true;
//		Scanner read = new Scanner(System.in);
//		Executor exec = Executors.newFixedThreadPool(2);
//		
//		/**
//		 * Menu programu.
//		 */
//		while(flag){
//			System.out.println("1. Ustaw temperatur�");
//			System.out.println("2. Ustaw wilgotno��");
//			System.out.println("3. Wyj�cie z programu");
//			
//			vCase = read.nextInt();
//			
//			switch (vCase){
//				case 1: {
//					System.out.println("Jak� temperatur� chcesz ustawi�? Obecnie jest: "+data.getCurrentTemperature()+"\"C");
//					keyboard = read.nextInt(); 
//					exec.execute(new TemperatureThread(data, keyboard));
//					try {
//						Thread.sleep(1000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					break;
//				}
//				
//				case 2: {
//					System.out.println("Jak� wilgotno�� chcesz ustawi�? Obecnie jest "+ data.getCurrentHumidity()+"%");
//					keyboard = read.nextInt();
//					exec.execute(new HumidityThread(data, keyboard));
//					try {
//						Thread.sleep(1000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					break;
//				}
//				
//				case 3: {
//					System.out.println("Exit");
//					flag = false;
//					break;
//				}
//				
//				default: {
//					System.out.println("Nieprawid�owy wyb�r. Spr�buj jeszcze raz");
//					break;
//				}
//			}
//			
//			
//		}
//		
//		
	}

}

