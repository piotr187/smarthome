package smarthome;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Humidity {

	private int currentHumidity;
	Lock lock = new ReentrantLock();
	Data data;
	
	public Humidity(Data data) {
		this.data = data;
	}
	
	public int checkCurrentHumidity(){
		lock.lock();
		try{
			currentHumidity = data.getCurrentHumidity();
			return currentHumidity;
		} finally{
			lock.unlock();
		}
	}
	
	public void doHumidity(int humidityAux){
		lock.lock();
		try{
			if(humidityAux < 20 || humidityAux > 90){
				System.out.println("Nieprawid�owa warto��. Wilgotno�� musi mie�ci� si� w przedziale od 20% do 90%");
			}
			else{
				if(data.getCurrentHumidity() == humidityAux){
					System.out.println("Wilgotno�� jest prawid�owa");
				} else if(data.getCurrentHumidity() < humidityAux){
					System.out.println("Wilgotno�� jest za niska. Podwy�szam wilgotno��.");
					data.setCurrentHumidity(humidityAux);
					}
				 else if(data.getCurrentHumidity() > humidityAux){
					System.out.println("Wilgotno�� jest za wysoka. Obni�am Wilgotno��.");
					data.setCurrentHumidity(humidityAux);
				}
			}
		} finally {
			lock.unlock();
		}
		
	}
	
}
