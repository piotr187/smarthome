package smarthome;

import java.io.Serializable;

public class Data implements Serializable{

	private static final long serialVersionUID = -3878815430066335284L;
	
	private int temperature;
	private int currentTemperature = 20;
	private int humidity;
	private int currentHumidity = 50;
	
	public Data(int temperature, int humidity) {
		this.currentTemperature = currentTemperature;
		this.currentHumidity = currentHumidity ;
	}
	
	public Data() {
	}
	
	public int getTemperature() {
		return temperature;
	}
	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}
	public int getCurrentTemperature() {
		return currentTemperature;
	}
	public void setCurrentTemperature(int currentTemperature) {
		this.currentTemperature = currentTemperature;
	}
	public int getHumidity() {
		return humidity;
	}
	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}
	public int getCurrentHumidity() {
		return currentHumidity;
	}
	public void setCurrentHumidity(int currentHumidity) {
		this.currentHumidity = currentHumidity;
	}

	 
	
}
