package smarthome;

public class HumidityThread implements Runnable{

	Humidity humidity;
	Data data;
	int humidityAux;
	
	public HumidityThread(Data data, int humidityAux) {
		this.data = data;
		this.humidity = new Humidity(data);
		this.humidityAux = humidityAux;
	}

	@Override
	public void run() {
		humidity.doHumidity(humidityAux);
	}

}
