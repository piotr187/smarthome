package smarthome;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Temperature {
	Lock lock = new ReentrantLock();
	Data data;	
	
	public Temperature(Data data) {
		this.data = data;	}
	
	public void doTemperature(int temperature){
		lock.lock();
		try{
			if(temperature < 17 || temperature > 30){
				System.out.println("Nieprawid�owa warto��. Temperatura musi mie�ci� si� w przedziale od 17\"C do 30\"C");
			}
			else{
				if(data.getCurrentTemperature() == temperature){
					System.out.println("Temperatura jest prawid�owa");
				} else if(data.getCurrentTemperature() < temperature){
					System.out.println("Temperatura jest za niska. Podwy�szam temperature.");
						data.setCurrentTemperature(temperature);
						System.out.println(data.getCurrentTemperature());
				} else if(data.getCurrentTemperature() > temperature){
					System.out.println("Temperatura jest za wysoka. Obni�am temperatur�.");
						data.setCurrentTemperature(temperature);
						System.out.println(data.getCurrentTemperature());
						
				}
			}
		} finally {
			lock.unlock();
		}
		
	}
	

}
