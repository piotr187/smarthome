package smarthome;

public class TemperatureThread implements Runnable{

	Temperature temperature;
	Data data;
	int temp;
	

	public TemperatureThread(Data data, int temp) {
		this.data = data;
		this.temperature = new Temperature(data);
		this.temp = temp;
	}


	public void run() {
		temperature.doTemperature(temp);
			}
	}
