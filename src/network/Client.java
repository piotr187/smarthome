package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import smarthome.Data;


public class Client {

	public static void main(String[] args) {

		int port = 54321;
		String ipAdress = "127.0.0.1";
		
		
		try {
			Socket socket = new Socket(ipAdress, port);
			PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			Scanner sc = new Scanner(System.in);
			String str;
			String strIn = null;
			Scanner in = new Scanner(socket.getInputStream());
			
			while(sc.hasNext()){ //sprawdzamy czy przysz�a jaka� wiadomo��
				str = sc.nextLine(); //zapisujemy j� do zmiennej str
				out.println("Wiadomo�� od klienta: " + str); 
				out.flush(); //wysy�anie wiadomo��i na serwer
				
				if(str.equals("exit")) {
					System.out.println("Wyjscie");
					break;
				}
				
				//drkowanie wiadomo�ci z serwera
				if(!(strIn = in.nextLine()).equals("")){
					System.out.println(strIn);
					strIn = null;
				}
					
				
			}
			
			//wysy�anie obiektu - na razie nieprzydatne
			ObjectOutputStream ous = new ObjectOutputStream(socket.getOutputStream());
			ous.writeObject(new Data());
			
			sc.close();
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
