package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

	public static void main(String[] args) {

		int port = 54321;
		ServerSocket serverSocket = null;
		
		try {

			serverSocket = new ServerSocket(port);
			ExecutorService exec = Executors.newCachedThreadPool();
			
			while(true){
				//oczekiwanie na zg�oszenie klienta
				Socket socket = serverSocket.accept();
				//tworzenie w�tku dla danego po��czenia i uruchomienie go
				exec.execute(new ServerThread(socket).getFt());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
