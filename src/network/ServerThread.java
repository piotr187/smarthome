package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import smarthome.Data;


public class ServerThread implements Callable<String>{

	Socket mySocket;	
	
	FutureTask<String> ft;
	
	public ServerThread(Socket mySocket){
		this.mySocket = mySocket;
		ft = new FutureTask<String>(this);
	}

	public FutureTask<String> getFt() {
		return ft;
	}


	@Override
	public String call(){
		String txt = mySocket.getInetAddress().getHostName();

		try {
			PrintWriter out = new PrintWriter(new OutputStreamWriter(mySocket.getOutputStream()));
			Scanner sc = new Scanner(System.in);
			String strOut = null;
			
			//odbieranie i drukowanie danych
			BufferedReader in = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
			String str;
			Data data = new Data(20, 50);
			
			while (!(str = in.readLine()).equals("exit")) { //sprawdzamy czy klient chce sko�czyc dzia�anie
				System.out.println(str);  //drukowanie wiadomo�ci od klienta
				if(str.equals("exit")) break;  //ko�czenie pracy serwera je�li klient wys�a� "exit"
				strOut = sc.nextLine();  //wpisywanie wiadomo�ci do klienta z klawiatury
				out.println("Wiadomo�� od SERWERA: " + strOut);
				out.flush();  //wysy�anie wiadomo�ci do klienta						
			}
			
			//odbi�r obiektu - na razie nieprzydatne
			ObjectInputStream ois = new ObjectInputStream(mySocket.getInputStream());
			data = (Data)ois.readObject();
			System.out.println("koniec pracy");
			System.out.println(data.getCurrentTemperature());
			
			mySocket.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return "Zrobione! Socket: " + txt + " is closed.";
	}
}
